'use strict';

/**
 * @ngdoc function
 * @name yeomanApp.controller:MycontrollerCtrl
 * @description
 * # MycontrollerCtrl
 * Controller of the yeomanApp
 */
angular.module('yeomanApp')
  .controller('MycontrollerCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
