'use strict';

/**
 * @ngdoc filter
 * @name yeomanApp.filter:myFilter
 * @function
 * @description
 * # myFilter
 * Filter in the yeomanApp.
 */
angular.module('yeomanApp')
  .filter('myFilter', function () {
    return function (input) {
      return 'myFilter filter: ' + input;
    };
  });
