'use strict';

/**
 * @ngdoc service
 * @name yeomanApp.myService
 * @description
 * # myService
 * Service in the yeomanApp.
 */
angular.module('yeomanApp')
  .service('Myservice', function Myservice() {
    // AngularJS will instantiate a singleton by calling "new" on this function
  });
